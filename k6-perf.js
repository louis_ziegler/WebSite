import { sleep } from 'k6';
import http from 'k6/http';

export let options = {
    duration: '2m',
    vus: 200,
};

export default function() {
    http.get('http://formgitlab-louis-test.surge.sh');
    sleep(1);
}
